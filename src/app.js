'use strict' 

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const app = express();
const router = express.Router();

// Conecta ao banco
mongoose.connect('mongodb+srv://balta:balta@cluster0-zfflu.azure.mongodb.net/test?retryWrites=true&w=majority',
    {
        useNewUrlParser: true, 
        useCreateIndex: true,
        useUnifiedTopology: true 
    });

// Carrega as rotas
const indexRoute = require('./routes/index');
const productRoute = require('./routes/product');

app.use(bodyParser.json()); // todo conteudo convertido em json
app.use(bodyParser.urlencoded({ extended: false })); // codifica as urls


app.use('/', indexRoute);
app.use('/products', productRoute);
 
module.exports = app;
