'use strict';

const mongoose = require('mongoose');

const productoSchema = new mongoose.Schema ({ //os schemas geram ids automaticos
    // propiedades
    title: {
        type: String,
        required: true,
        trim: true //remove os espaços antes e depois do titulo
    },
    slug: {
        type: String,
        required: [true, 'O slug é obrigatório'],
        trim: true, 
        index: true, // indice, pra facilitar busca
        unique: true
    },
    description: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true 
    },
    active: {   
        type: Boolean,
        required: true,
        default: true 
    },
    tags: [{
        type:  String,
        required: true 
    }]
});

module.exports = mongoose.model('Product', productoSchema);